
enum {
	op_var,
	op_int,
	op_op,
};

enum {
	gen_pair,
	gen_set,
};

struct input {
	struct var_val *vv;
	int n;
	int out;
};

#define MAX_ARG 2
struct op {
	char *name;
	int type;
	int argc;
	struct op *argv[MAX_ARG];
	int value;
	union {
		int (*eval1)(int);
		int (*eval2)(int,int);
	};
	struct op * next, *prev;
	int gen_type;
};


struct op* clone(struct op*op){
	struct op * out = malloc(sizeof(struct op));
	int i;

	*out = *op;

	out->next = NULL;
	out->prev = NULL;

	for(i=0;i<MAX_ARG;++i){
		out->argv[i] = NULL;
	}
	return out;
}



void join(struct op*a , struct op*b){
	a->next = b;
	b->prev = a;
}




struct op* new_int(int a){
	struct op* op= malloc(sizeof(struct op));
	op->type = op_int;
	op->value = a;
	return op;
}

struct op* new_var(char *name){
	struct op* op = malloc(sizeof(struct op));
	op->type = op_var;
	op->name = name;
	return op;
}

struct op* new_op1(char *name, struct op*a, int (*eval)(int)){
	struct op* op = malloc(sizeof(struct op));
	op->type = op_op;
	op->argc = 1;
	op->name = name;
	op->argv[0] = a;
	op->argv[1] = NULL;
	op->eval1 = eval;
	return op;
}

struct op* new_op2(char *name, struct op*a, struct op*b, void *fun ){
	struct op* op = malloc(sizeof(struct op));
	op->name = name;
	op->argc = 2;
	op->argv[0] = a;
	op->argv[1] = b;
	op->type = op_op;
	op->eval2 = fun;
	return op;
}


struct var_val {
	int value;
	char *name;
};


int eval(struct op * op, struct var_val *set,int n){
	int i;
	switch(op->type){
		case op_int:
			return op->value;
			break;
		case op_var:
			for(i=0;i<n;++i){
				if(!strcmp(set[i].name, op->name)){
					return set[i].value;
				}
			}
			printf("ERROR: cannot evaluate, variable '%s' unknown\n",op->name);
			exit(1);
			break;
		case op_op:
			switch(op->argc){
				case 1:
					return op->eval1( eval(op->argv[0], set,n ));
				case 2:
					return op->eval2( eval(op->argv[0], set,n), eval(op->argv[1],set,n) );
				default:
					printf("ERROR: more than 2 arguments not supported\n");
					exit(1);
					break;
			}		
			break;
	}
}




struct input* read_input_onevar(char *fname, char * varname, int *n){
	FILE * f = fopen(fname,"r");
	if( f == NULL ){
		printf("ERROR: cannot open file '%s'\n",fname);
		exit(1);
	}

	int lines;
	int i;
	int intin,intout;
	struct input *in;


	fscanf(f,"%i",&lines);

	in = malloc(sizeof(struct input) * lines);
	if( in == NULL ) { 
		printf("ERROR: canno alloc mamory for input \n");
		exit(1);
	}

	for(i=0;i<lines;++i){
		fscanf(f,"%i %i",&intin, &intout);
		struct var_val *vv;


		vv = malloc(sizeof(struct var_val));
		if( vv == NULL ) { 
			printf("ERROR: cannot allocate memory for var_var\n");
			exit(1);
		}

		vv->name = varname;
		vv->value = intin;
		in[i].n=1;
		in[i].out = intout;
		in[i].vv = vv;

	}	
	
	*n = lines;
	fclose(f);

	return in;
}


int check_op(struct op*op, struct input *in, int inn){
	
	int i;
	for(i=0;i<inn;++i){
		struct var_val *vv = in[i].vv;
		int out = in[i].out;
		int n = in[i].n;
		int computed = eval(op, vv, n);
		if(computed != out ) 
			return 0;
	}
	return 1;
}

