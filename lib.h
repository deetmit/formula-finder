
int op_add(int a,int b){
	return a+b;
}
int op_del(int a,int b){
	return a-b;
}
int op_mul(int a,int b){
	return a*b;
}
int op_shift_left(int a,int b){
	return a<<b;
}
int op_shift_right(int a,int b){
	return a>>b;
}
int op_and(int a,int b){
	return a & b;
}
int op_or(int a,int b){
	return a|b;
}
int op_not(int a){
	return ~a;
}


struct op* new_add(struct op*a, struct op*b){
	struct op * r =  new_op2("+",a,b,op_add);
	r->gen_type = gen_set;
	return r;
}
struct op* new_del(struct op*a,struct op*b){
	struct op*r = new_op2("-",a,b,op_del);
	r->gen_type = gen_pair;
	return r;
}
struct op* new_mul(struct op*a,struct op*b){
	struct op* r=  new_op2("*",a,b,op_mul);
	r->gen_type = gen_set;
	return r;
}

struct op* new_shift_left(struct op*a,struct op*b){
	struct op* r =  new_op2("<<",a,b,op_shift_left);
	r->gen_type = gen_pair;
	return r;
}
struct op* new_shift_right(struct op*a,struct op*b){
	struct op *r= new_op2(">>",a,b,op_shift_right);
	r->gen_type = gen_pair;
	return r;
}
struct op* new_and(struct op*a,struct op*b){
	struct op * r = new_op2("&",a,b,op_and);
	r->gen_type = gen_set;
	return r;
}
struct op* new_or(struct op*a,struct op*b){
	struct op* r =  new_op2("|",a,b,op_or);
	r->gen_type = gen_set;
	return r;
}
struct op* new_not (struct op*a){
	return new_op1("not",a,op_not);
}

