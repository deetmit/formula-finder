Simple program that finds  formula(s) for input-output values. 

Compilation.
Just run 'make'.


Run.

./op <file_name>


File format.

<number of lines>
<input1> <output1>
<input2> <outupt2>
...


eg.

3
1 2
2 3
3 4

For this file, the formula would be f(x) = x+1.


