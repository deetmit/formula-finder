
struct pair {
	struct op *start,*end;
};

struct pair join_op(struct pair a, struct op*op){
	struct pair out;
	
	out.start = a.start;
	out.end = op;

	op->prev = a.end;
	a.end -> next = op;

	return out;
	
}

struct pair join_pair(struct pair a, struct pair b){
	struct pair out;

	out.start = a.start;
	out.end = b.end;

	join(a.end,b.start);

	return out;
}



struct pair op_to_pair(struct op*op){
	struct pair p;
	p.start = op;
	p.end = op;
	return p;
}

struct pair create_range(int a,int b){
	struct pair p;
	int i;
	struct op*prev,*cur;

	cur = new_int(a);
	p.start = cur;

	i=a+1;
	while(i<=b){
		prev= cur;
		cur = new_int(i);
		cur->prev = prev;
		prev->next = cur;
		++i;
	}
	p.end = cur;
	return p;

}

