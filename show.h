
void show(struct op* op){
	int i;
	if(op == NULL ) {
		printf("<n>");
		return ;
	}
	switch(op->type){
		case op_int:
			printf("%i",op->value);
			break;
		case op_var:
			printf("%s",op->name);
			break;
		case op_op:
			
			switch(op->argc){
				case 1:
					printf("%s ",op->name);
					show(op->argv[0]);
					break;
				case 2:
				        printf("(");	
					show(op->argv[0]);
					printf(" %s ",op->name);
					show(op->argv[1]);
					printf(")");
					break;
				default:
					printf("(");
					for(i=0;i<op->argc;++i){
						show(op->argv[i]);
						if( i != op->argc-1 )
							printf(" , ");
					}
					printf(")");
					break;
			}
			break;
	}
}
void shown(struct op*op){
	show(op);
	printf("\n");
}

void show_pair(struct pair p){
	struct op*iter = p.start;
	
	if( (p.start == NULL) || (p.end == NULL ) ) {
		printf("<empy pair>\n");
		return;
	}
	show(iter);
	while(iter != p.end){
		iter = iter->next;
		printf("\n");
		show(iter);
	}
	printf("\n");

}

void show_break(){
	printf("======================\n");
}
