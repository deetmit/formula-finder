
void show(struct op*);

struct pair generate2_type_and_check(struct op*op, struct pair ina, struct pair inb, int gen_type,
		struct input *incheck, int inn){
	struct op* iter1,*iter2;
	struct op* newop, *prevop;
	struct pair out;

	iter1=ina.start;
	iter2=inb.start;


	out.start = NULL;
	out.end = NULL;


	newop = NULL;
	prevop = NULL;


	iter2= inb.start;
	while(1){
		switch(gen_type){
			case gen_set:
				// UWAGA: tylko przy gen_pari
				if( !( (ina.start == inb.start) && (ina.end == inb.end) ) ){
					printf("ERROR: used gen_type := gen_set in generation2 with ina!=inb\n");
					exit(1);
				}
				iter1=iter2;
				break;
			case gen_pair:
				iter1=ina.start;
				break;
			default:
				printf("ERROR: not supported gen_type(%i)\n",op->gen_type);
				exit(1);
				break;
		}

		while(1){
			newop = clone(op);
			newop->argv[0] = iter1;
			newop->argv[1] = iter2;
			if( out.start == NULL ) out.start = newop;
			out.end = newop;		
			
			int check = check_op(newop, incheck,inn);
			if( check == 1 ) {
				printf("found op: ");
				show(newop);
				printf("\n");
			}

			newop->prev=prevop;
			if(prevop != NULL ) prevop->next = newop;

			prevop = newop;
			if( iter1 == ina.end ) break;
			iter1=iter1->next;

		}
		if( iter2 == inb.end ) break;
		iter2=iter2->next;
	
	}
	return 	out;
}


struct pair generate1_and_check(struct op*op,struct pair in, struct input* incheck, int inn){
	struct op* iter;
	struct op* newop,*prevop;
	struct op* start;
	struct pair p;

	iter = in.start;
	newop = clone(op);
	start = newop;
	newop->argv[0] = iter;


	if(iter == in.end ){
		p.start = start;
		p.end = start;
		return p;
	}
	iter = iter->next;
	

	while(1){
		prevop = newop; 
		newop = clone(op);
		newop->argv[0] = iter;
	        newop->prev = prevop;
		prevop->next = newop;

		int check = check_op(newop, incheck,inn);
		if( check == 1 ) {
			printf("found op: ");
			show(newop);
			printf("\n");
		}

		
		if(iter == in.end ) 
			break;
		iter = iter->next;
	
	}
	p.start = start;
	p.end = newop;
	return p;
}

struct pair generate2_and_check(struct op*op, struct pair in,
		struct input*incheck, int inn){
	return generate2_type_and_check(op,in,in, op->gen_type,incheck,inn);
}

struct pair generate_and_check(struct op*op, struct pair in,
		struct input *incheck, int inn){
	if( op->type != op_op ){
		printf("ERROR: op must be of type op_op (is %i)\n",op->type);
		exit(1);
	}		
	switch (op->argc) {
		case 1:
			return generate1_and_check(op,in,incheck,inn);
			break;
		case 2:
			return generate2_and_check(op,in,incheck,inn);
			break;
		default:
			printf("ERROF: more than 2 args not supported yet\n");
			exit(1);
			break;
	}
}
/*
struct pair generate_cross(struct op*op, struct pair ina, struct pair inb){
	if(op->argc != 2 ){
		printf("ERROR: op argc !=2, cannot call generate_cross()\n");
		exit(1);
	}
	return generate2_type(op,ina,inb,gen_pair);
}
*/
struct pair generate_cross_and_check(struct op*op, struct pair ina, struct pair inb,
		struct input *incheck, int inn){
	if(op->argc != 2 ){
		printf("ERROR: op argc !=2, cannot call generate_cross()\n");
		exit(1);
	}
	return generate2_type_and_check(op,ina,inb,gen_pair,incheck, inn);
}
/*
struct pair generate_unique(struct op*op, struct pair in, struct pair inconst){
	struct pair mix,cross,out;


	mix = generate(op,in);

	if( op->argc !=2 ){
		out = mix;
	}else{
	
		cross = generate_cross(op,in,inconst);

		out = join_pair(mix,cross);
	}

	return out;
}
*/
struct pair generate_unique_and_check(struct op*op, struct pair in, struct pair inconst,
		struct input *incheck, int inn){
	struct pair mix,cross,out;


	mix = generate_and_check(op,in,incheck,inn);

	if( op->argc !=2 ){
		out = mix;
	}else{
	
		cross = generate_cross_and_check(op,in,inconst,incheck,inn);

		out = join_pair(mix,cross);
	}

	return out;
}

/*
struct pair generate_depth1( struct pair ops, struct pair in, struct pair inconst){
	struct pair out,tmp;
	struct op * op;

	op = ops.start;
	out = generate_unique(op,in,inconst);

	if( op == ops.end ){
		return out;
	}
	op = op->next;

	while(1){
		tmp  = generate_unique(op,in,inconst);
		out = join_pair(out,tmp);
		if( op == ops.end ){
			return out;
		}
		op = op->next;
	}
	return out; // shoul never be there

}
*/

struct pair generate_depth1_and_check( struct pair ops, struct pair in, struct pair inconst,
		struct input *incheck, int inn){
	struct pair out,tmp;
	struct op * op;

	op = ops.start;
	out = generate_unique_and_check(op,in,inconst,incheck,inn);

	if( op == ops.end ){
		return out;
	}
	op = op->next;

	while(1){
		tmp  = generate_unique_and_check(op,in,inconst,incheck,inn);
		out = join_pair(out,tmp);
		if( op == ops.end ){
			return out;
		}
		op = op->next;
	}
	return out; // shoul never be there

}


/*
struct pair generate_depth(int depth, struct pair ops, struct pair in, struct pair inconst){

	struct pair out;
	if( depth == 0 ){
		return in;
	}else{
		return generate_depth(depth-1,ops, generate_depth1(ops,in,inconst), inconst);
	}


}
*/
struct pair generate_depth_and_check(int depth, struct pair ops, struct pair in, struct pair inconst, 
		struct input *incheck, int inn){

	struct pair out;
	if( depth == 0 ){
		return in;
	}else{
		return generate_depth_and_check(depth-1,ops, 
				generate_depth1_and_check(ops,in,inconst,incheck,inn), 
				inconst,incheck,inn);
	}


}



