#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "op.h"
#include "pair.h"
#include "lib.h"
#include "generate.h"
#include "show.h"

int main(int argc,char *argv[]){
	/* 
	 * read data from file
	 */

	int input_lines;
	struct input * input_data;

	input_data  = read_input_onevar(argv[1],"x",&input_lines);

	/*
	 * create variable 'x'
	 */
	struct op * x = new_var("x");
	struct pair vars = op_to_pair(x);


	/*
	 * create range of numbers (constants in the formula, eg. (x & 0x07) )
	 */
	struct pair ints = create_range(0,0xf);


	/*
	 * add operations supported
	 */
	struct pair ops;
	ops = op_to_pair(new_not(NULL));
	ops = join_pair(ops, op_to_pair(new_add(NULL,NULL)));
	ops = join_pair(ops, op_to_pair(new_del(NULL,NULL)));
	ops = join_pair(ops, op_to_pair(new_mul(NULL,NULL)));
	ops = join_pair(ops, op_to_pair(new_shift_left(NULL,NULL)));
	ops = join_pair(ops, op_to_pair(new_shift_right(NULL,NULL)));
	ops = join_pair(ops, op_to_pair(new_and(NULL,NULL)));
	ops = join_pair(ops, op_to_pair(new_or(NULL,NULL)));


	/*
	 * set depth, 
	 * warning, big number (like 6 may consume huge amount of ram)
	 */
	struct pair out = generate_depth_and_check(3, ops, vars, ints, input_data, input_lines);

	return 0;
}
